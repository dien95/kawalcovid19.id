import * as React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import styled from '@emotion/styled';
import VisuallyHidden from '@reach/visually-hidden';

import { themeProps, Box, UnstyledAnchor, UnstyledButton } from 'components/ui-core';
import { logEventClick } from 'utils/analytics';
import useDarkMode from 'utils/useDarkMode';
import Logo from './Logo';
import NavLink from './NavLink';
import SearchModal from './SearchModal';
import { NavGrid, NavInner } from './NavComponents';
// import SearchIcon from './SearchIcon';

const StyledHeader = Box.withComponent('header');

const Root = styled(StyledHeader)``;

const Left = styled('div')`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 60px;
  padding: 00;
  user-select: none;

  ${themeProps.mediaQueries.lg} {
    padding: 0 24px;
    padding-left: 0;
  }
`;

const Center = styled('div')`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  height: 60px;
  padding: 0;
  user-select: none;
  flex: 1 1 auto;

  ${themeProps.mediaQueries.lg} {
    padding: 0 24px;
    padding-left: 0;
  }
`;

// const Right = styled('nav')`
//   display: flex;
//   align-items: center;
//   justify-content: center;
//   height: 60px;

//   ${themeProps.mediaQueries.lg} {
//     justify-content: flex-end;
//   }
// `;

const ToggleButton = styled(UnstyledButton)`
  outline: none;
`;

// const SearchButton = styled(UnstyledButton)`
//   display: inline-flex;
//   align-items: center;
//   justify-content: center;
//   width: 40px;
//   height: 40px;
// `;

const Navigation: React.FC = () => {
  const [, /* isDarkMode */ toggleDarkMode] = useDarkMode();
  const [isSearchModalOpen, setIsSearchModalOpen] = React.useState(false);
  const router = useRouter();

  const toggleSearchModal = () => {
    setIsSearchModalOpen(!isSearchModalOpen);
  };

  return (
    <Root>
      <NavGrid backgroundColor="background" color="foreground">
        <NavInner display="flex" flexDirection="row">
          <Left>
            <Link href="/" passHref>
              <UnstyledAnchor
                display="inline-flex"
                alignItems="center"
                onClick={() => logEventClick('Beranda')}
                fontWeight={600}
              >
                <VisuallyHidden>Kawal COVID-19</VisuallyHidden>
                <Logo aria-hidden />
              </UnstyledAnchor>
            </Link>
          </Left>
          <Center>
            <ToggleButton
              type="button"
              ml="sm"
              backgroundColor="accents01"
              color="foreground"
              py="xxs"
              px="xs"
              borderRadius={2}
              onClick={toggleDarkMode}
            >
              Ganti mode warna
            </ToggleButton>
          </Center>
          {/* <Right>
            <SearchButton type="button" backgroundColor="accents01" onClick={toggleSearchModal}>
              <SearchIcon fill={isDarkMode ? '#f1f2f3' : '#22272c'} />
            </SearchButton>
          </Right> */}
        </NavInner>
      </NavGrid>
      <NavGrid backgroundColor="accents02" color="foreground">
        <Box
          as="nav"
          display="flex"
          flexDirection="row"
          gridColumn="3/4"
          overflowX="auto"
          overflowY="hidden"
        >
          <NavLink href="/faq" isActive={router.pathname === '/faq'} title="FAQ" />
          <NavLink
            href="/unduh-aplikasi"
            isActive={router.pathname === '/unduh-aplikasi'}
            title="Unduh Aplikasi"
          />
          <NavLink
            href="/tentang-kami"
            isActive={router.pathname === '/tentang-kami'}
            title="Tentang Kami"
          />
          <NavLink
            href="/category/[slug]"
            as="/category/informasi"
            isActive={router.pathname === '/category/informasi'}
            title="Berita"
          />
          <NavLink
            href="/category/[slug]"
            as="/category/verifikasi"
            isActive={router.pathname === '/category/verifikasi'}
            title="Periksa Fakta"
          />
          <NavLink
            href="/category/[slug]"
            as="/category/kegiatan-keluarga"
            isActive={router.pathname === '/category/kegiatan-keluarga'}
            title="Kegiatan Keluarga"
          />
        </Box>
      </NavGrid>
      <SearchModal isOpen={isSearchModalOpen} onClose={toggleSearchModal} />
    </Root>
  );
};

export default Navigation;
